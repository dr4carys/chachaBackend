# chachaBackend
# sebelum menjalankan kode pastikan sudah terinstall flask, python dan flask_mysqldb

install flask: pip install flask 

install flask_mysqldb : pip install flask-mysqldb

pastikan sudah import db_chachabackend 

# link POSTMAN https://www.getpostman.com/collections/29675dc38ff4956fba91
# pertama ubah main app flask dari app.py menjadi main.py menggunakan syntax berikut : set FLASK_APP=main.py
# selanjutnya jalankan flask dengan syntax berikut : flask run
# lalu API dapat diaskes melalui postman
